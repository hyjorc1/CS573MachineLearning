##
#Author: Yijia Huang
#Date: 03/26/2018
##

################################################################# Task 1

########### Random Forest
library(caret)
library(randomForest)
train <- read.csv("C:/Users/hyj/git/CS573 Machine Learning/EnsembleLearning/lab3-train.csv")
test <- read.csv("C:/Users/hyj/git/CS573 Machine Learning/EnsembleLearning/lab3-test.csv")

train$Class <- as.factor(train$Class)
test$Class <- as.factor(test$Class)
test_Y <- test$Class

# Extend Caret with my customRF
customRF <- list(type = "Classification", library = "randomForest", loop = NULL)
customRF$parameters <- data.frame(parameter = c("mtry", "ntree"), class = rep("numeric", 2), label = c("mtry", "ntree"))
customRF$grid <- function(x, y, len = NULL, search = "grid") {}
customRF$fit <- function(x, y, wts, param, lev, last, weights, classProbs, ...) {
  randomForest(x, y, mtry = param$mtry, ntree=param$ntree, ...)
}
customRF$predict <- function(modelFit, newdata, preProc = NULL, submodels = NULL)
  predict(modelFit, newdata)
customRF$prob <- function(modelFit, newdata, preProc = NULL, submodels = NULL)
  predict(modelFit, newdata, type = "prob")
customRF$sort <- function(x) x[order(x[,1]),]
customRF$levels <- function(x) x$classes


# rf grid of tuning parameters
rf_grid <- expand.grid(.mtry=c(1:10),
                       .ntree=c(1000,1500,2000,2500))
# rf control
rf_control <- trainControl(method="repeatedcv",
                           # 10-fold CV
                           number=10,
                           # repeated ten times
                           repeats=10)

rf <- train(Class~.,
            data=train, 
            method=customRF,
            trControl=rf_control,
            metric="Accuracy",
            tuneGrid=rf_grid)
pred_Y <- predict(rf,test)

# rf model
rf
# plot model
plot(rf)
# cf matrix
confusionMatrix(data = pred_Y,test_Y)


############ AdaBoost.M1
library(caret)
library(adabag)
library(plyr)

# ada grid of tuning parameters
ada_grid <- expand.grid(mfinal = c(100,200,400,800), 
                        maxdepth = c(1,10,20,100),
                        coeflearn = c("Breiman", "Freund", "Zhu"))

# ada control
ada_control <- trainControl(method = "cv", 
                            number = 3,
                            returnResamp = "all")

ada <- train(Class~.,
             train, 
             method="AdaBoost.M1",
             trControl = ada_control,
             tuneGrid = ada_grid,
             metric = "Accuracy")
pred_Y <- predict(ada,test)

# ada model
ada
# plot model
plot(ada)
# ada matrix
confusionMatrix(data = pred_Y,test_Y)
mean(pred_Y==test_Y)
############################################################# Task 2

########### NN
control <- trainControl(method="repeatedcv",
                        # 10-fold CV
                        number=10,
                        # repeated ten times
                        repeats=10)

nn_grid <- expand.grid(.decay = c(0.1), 
                       .size = c(5))
nn <- train(Class~.,
            data =train, 
            method="nnet", 
            maxit=1000000000,
            trControl=control,
            tuneGrid=NULL)
pred_Y_nn <- predict(nn,test)

nn

plot(nn)

confusionMatrix(pred_Y_nn,test_Y)

########### KNN

knn <- train(Class~.,
             train,
             method="knn",
             trControl=control,
             preProcess = c("center","scale"),
             tuneLength=20)
pred_Y_knn <- predict(knn,test)

knn

plot(knn)

confusionMatrix(pred_Y_knn,test_Y)


########### LR
lr <- train(Class ~ ., 
            train, 
            method = "glm",
            metric = "Accuracy",
            preProcess = c("center","scale"),
            trControl = control)
pred_Y_lr <- predict(lr,test)

lr

plot(lr)

confusionMatrix(pred_Y_lr,test_Y)

########### NB
nb_grid <- expand.grid(fl=c(0,0.5,1.0),
                       adjust=c(0,0.5,1.0),
                       usekernel=c("FALSE", "TRUE"))
nb <- train(Class~.,
            data =train, 
            method = "nb", 
            trControl = control,
            metric = "Accuracy",
            tunegrid=nb_grid)
pred_Y_nb <- predict(nb,test)

nb

plot(nb)

confusionMatrix(pred_Y_nb,test_Y)


########### DT
dt_control <- trainControl(method = "repeatedcv", number = 10, repeats = 3)
dt <- train(Class~.,data =train, method = "rpart",
            trControl = dt_control,
            parms = list(split = "information"),
            tuneLength = 10)
pred_Y_dt <- predict(dt,test)

dt

plot(dt)

confusionMatrix(pred_Y_dt,test_Y)

########### Ensemble Classifier with unweighted majority vote over 5 models

library(mlbench)
library(devtools)
install_github('caretEnsemble', 'zachmayer') #Install zach's caretEnsemble package
library(caretEnsemble)

#Make a list of all the models
all.models <- list(nn, knn, lr, nb, dt)
names(all.models) <- sapply(all.models, function(x) x$method)
sort(sapply(all.models, function(x) min(max(x$results$Accuracy))))

models <- data.frame(pred_Y_nn,pred_Y_knn,pred_Y_lr,pred_Y_nb,pred_Y_dt)
bagging <- function(x) {
  table <- table(x)
  sort <- sort(table, decreasing=TRUE)
  majority <- names(sort)[1]
}

pred_majority <- apply(models, 1, bagging)
confusionMatrix(pred_majority,test_Y)
########### Ensemble Classifier with weighted majority vote over 5 models

pred_nn <- as.numeric(pred_Y_nn) - 1
pred_knn <- as.numeric(pred_Y_knn) - 1
pred_lr <- as.numeric(pred_Y_lr) - 1
pred_nb <- as.numeric(pred_Y_nb) - 1
pred_dt <- as.numeric(pred_Y_dt) - 1

pred_boosting <- 0.8*pred_nn + 0.005*pred_knn + 0.05*pred_lr + 0.05*pred_nb + 0.005*pred_dt

boosting <- as.factor(ifelse(pred_boosting > 0.5, 1, 0))
confusionMatrix(boosting,test_Y)
########### Ensemble Classifier with unweighted majority vote over 7 models
models2 <- data.frame(pred_Y_nn,pred_Y_knn,pred_Y_lr,pred_Y_nb,pred_Y_dt, pred_Y_ada, pred_Y_lr)
pred_majority2 <- apply(models2, 1, bagging)
confusionMatrix(pred_majority2,test_Y)
########### Ensemble Classifier with weighted majority vote over 7 models
pred_lr <- as.numeric(pred_Y_lr) - 1
pred_ada <- as.numeric(pred_Y_ada) - 1
pred_boosting2 <- 0.5*pred_nn + 0.15*pred_knn + 0.05*pred_lr + 0.05*pred_nb + 0.005*pred_dt + 0.05*pred_lr + 0.05*pred_ada
boosting2 <- as.factor(ifelse(pred_boosting2 > 0.5, 1, 0))
confusionMatrix(boosting2,test_Y)