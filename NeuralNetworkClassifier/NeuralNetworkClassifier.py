# Yijia Huang
# 03/09/2018 
#
# coding: utf-8

# In[528]:


import h2o
from h2o.estimators.deeplearning import H2ODeepLearningEstimator
from h2o.grid.grid_search import H2OGridSearch
h2o.init()
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Activation, Convolution2D, MaxPooling2D
from keras.optimizers import SGD
from keras.constraints import maxnorm
from keras.utils import np_utils
from keras.optimizers import RMSprop
from keras.callbacks import EarlyStopping
 
# download data from remote database
train_df = h2o.import_file(path="https://archive.ics.uci.edu/ml/machine-learning-databases/optdigits/optdigits.tra")
test_df = h2o.import_file(path="https://archive.ics.uci.edu/ml/machine-learning-databases/optdigits/optdigits.tes")

y = "C65"
x = train_df.names[0:64]

train_df[y] = train_df[y].asfactor()
test_df[y] = test_df[y].asfactor()

# use 20% train data as the validation set
splits = train_df.split_frame(ratios=[0.2],seed=11111)
train = splits[1]
valid = splits[0]


# In[529]:


# nn model
def train_model(loss = "CrossEntropy", activation = "Rectifier"):

    model = H2ODeepLearningEstimator(distribution = "multinomial",
                                    
                                    # ReLU vs Tanh
                                    activation = activation, 
                                    
                                    # three hidden units or more
                                    hidden = [32,32,32], 
                                    
                                    adaptive_rate = False, 

                                    # Quadratic vs CrossEntropy
                                    loss = "CrossEntropy", 
                                    
                                    # learning rate
                                    rate = 0.005, 
                                    
                                    # Too much momentum can lead to instability. 
                                    # Incrementing the momentum slowly as recommended.
                                    momentum_start = 0.5,
                                    momentum_ramp = 1000000,  
                                    momentum_stable = 0.99,
                                    
                                    # use Nesterov Accelerated Gradient
                                    nesterov_accelerated_gradient = True, 
                                    
                                    # 0.1 or 0.2 as suggested
                                    input_dropout_ratio = 0.1, 
                                    
                                    # Enable sparse data handling, 
                                    # which is more efficient for data with many zero values.
                                    sparse = True,
                                    
                                    l1 = 1e-5, # Add regularization to add stability and improve generalization. 
                                               # Sets the value of many weights to 0.
                                    l2 = 1e-5, # Sets the value of many weights to smaller values.
                                    
                                    # misclassification vs AUC vs logloss vs rounds = 0
                                    stopping_rounds = 3,  
                                    stopping_metric = "misclassification", 
                                    stopping_tolerance = 0.01,
                                    
                                    variable_importances = True,
                                    
                                    epochs = 1000)
 
    model.train(x = x,
                y = y,
                training_frame = train,
                validation_frame = valid)

    return model


# In[530]:


# grid search for top 5 nn models
def train_model_grid(loss = "CrossEntropy"):

    hidden_opt = [[40,20],[40,30,20],[100],[50,40,30,20]]
    l1_opt = [s/1e6 for s in range(1,1001)]
    l2_opt = [s/1e6 for s in range(1,1001)]
    rate_opt = [s*0.001 for s in range(1,11)]
    act_opt = ['Rectifier','Tanh']

    hyper_parameters = {"hidden":hidden_opt, 
                        "l1":l1_opt,
                        "l2":l2_opt,
                        "rate":rate_opt,
                        "activation":act_opt}

    search_criteria = {"strategy":"RandomDiscrete",
                        "max_models":5, 
                        "max_runtime_secs":1000,
                        "seed":111}

    model_grid = H2OGridSearch(H2ODeepLearningEstimator, 
                            hyper_params = hyper_parameters,
                            search_criteria = search_criteria)


    model_grid.train(x=x, 
                    y=y,
                    distribution="multinomial", 
                    epochs=1000,
                    training_frame=train, 
                    validation_frame=valid,
                    score_interval=2,
                    
                    adaptive_rate = False,

                    stopping_rounds = 5,
                    stopping_tolerance = 0.01,
                    stopping_metric = "misclassification",
                    
                    momentum_start = 0.4,
                    momentum_ramp = 1000000,  
                    momentum_stable = 0.99,
                    
                    # Quadratic vs CrossEntropy
                    loss = loss)
    
    return model_grid


# In[531]:


def model_analysis(model=None, speed = False, actparams = False):

    # Retrieve the scoring history
    if speed: 
        print("Top 5 models:")
        print(model._model_json['output']['scoring_history'].as_data_frame())

    # train performance
    print(model.model_performance(train=True))

    # valid performance
    print(model.model_performance(valid=True))
    
    # test performance
    print(model.model_performance(test_df))
    
    # model actual parameters
    if actparams:
        print('\n model actual parameters')
        print(model.actual_params)


# In[532]:


##############################################################################################################################


# In[533]:


# Part 1 (a)
#
# model with cross-entropy
print('\nmodel with ReLU and cross-entropy error function')
model = train_model()
model_analysis(model=model, speed = True)


# In[534]:


# model with sum-of-squares 
print('\nmodel with ReLU and sum-of-squares error function')
model = train_model(loss = 'Quadratic')
model_analysis(model=model, speed = True)


# In[535]:


# best model with least mse
model_grid = train_model_grid()
model_gridperf = model_grid.get_grid(sort_by='mse', decreasing=False)
print(model_gridperf)
model_analysis(model=model_gridperf.models[0], actparams = True)


# In[536]:


#############################################################################################################################


# In[537]:


# Part 1 (b)
#
# model with tanh
print('\nmodel with tanh and cross-entropy error')
model = train_model(activation = 'Tanh')
model_analysis(model=model, speed = True)


# In[538]:


# best model with least mse
model_grid = train_model_grid()
model_gridperf = model_grid.get_grid(sort_by='mse', decreasing=False)
print(model_gridperf)
model_analysis(model=model_gridperf.models[0], actparams = True)


# In[539]:


############################################################################################################## End of Part 1


# In[540]:
# Part 2
# In[541]:


n = train_df.as_data_frame().as_matrix()


# In[542]:


x = train_df.as_data_frame().as_matrix() 


# In[543]:


# keras train set
k_train = train.as_data_frame().as_matrix()
X_training = k_train[:,:64]
y_training = k_train[:,64]
# keras valid set
k_valid = valid.as_data_frame().as_matrix()
X_validing = k_valid[:,:64]
y_validing = k_valid[:,64]
# keras test set
k_test = test_df.as_data_frame().as_matrix()
X_testing = k_test[:,:64]
y_testing = k_test[:,64]


# In[544]:


def normalize(X):
    X_s = StandardScaler().fit_transform(X)
    X_m = np.mean(X_s, axis=0)
    X_c = (X_s - X_m).T.dot((X_s - X_m)) / (X_s.shape[0]-1)    
    return X_s, X_m, X_c

X_train, _, _ = normalize(X_training)
X_valid, _, _ = normalize(X_validing)
X_test, _, _ = normalize(X_testing)

X_train = X_train.reshape(-1,8,8,1)
X_valid = X_valid.reshape(-1,8,8,1)
X_test = X_test.reshape(-1,8,8,1)

y_train = np_utils.to_categorical(y_training, 10)
y_valid = np_utils.to_categorical(y_validing, 10)
y_test = np_utils.to_categorical(y_testing, 10)


# In[545]:

# define layers
model = Sequential()
model.add(Convolution2D(32, 
                 (3, 3), 
                 input_shape=(8, 8, 1), 
                 activation="relu", 
                 padding="valid")) 
model.add(Activation('relu'))
model.add(Convolution2D(32, 
                 (3, 3), 
                 padding="same"))
model.add(MaxPooling2D(pool_size=(2, 2), 
                       data_format="channels_first")) 
model.add(Dropout(0.3))
model.add(Flatten())
model.add(Dense(128, 
                activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(10, 
                activation='softmax'))


# In[546]:
# define early stop
earlystop = EarlyStopping(monitor='val_acc', 
                          min_delta=0.0001, 
                          patience=5, 
                          verbose=1, 
                          mode='auto')
callbacks_list = [earlystop]

from keras import losses
model.compile(optimizer='adadelta', 
              loss=losses.categorical_crossentropy, 
              metrics=['accuracy'])


# In[549]:
hist = model.fit(X_train,y_train, 
                 batch_size=128, 
                 epochs=10, 
                 callbacks=callbacks_list,
                 validation_data=(X_valid,y_valid))

scores = model.evaluate(X_test, y_test, verbose=0)


# In[550]:


print(scores)

