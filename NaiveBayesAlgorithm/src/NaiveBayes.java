import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.IntStream;

/**
 * 
 * @author Yijia Huang
 *
 */
public class NaiveBayes {

	NewsGroup[] groups;

	Document[] docs;

	int totalNumOfDocs;

	int vocSize;

	double[][] MLE;

	double[][] BE;

	int[][] CM = new int[20][20];

	BufferedReader br;

	String line;

	HashMap<Integer, HashMap<Integer, Integer>> trainData = new HashMap<Integer, HashMap<Integer, Integer>>();

	public void learnTrainingData(String labelFilePath, String dataFilePath, String vocFilePath) throws IOException {

		try {
			
			loadLableFile(labelFilePath);
			loadVocFile(vocFilePath);
			loadDataFile(dataFilePath);
			
			// calculate MLE and BE
			MLE = new double[vocSize][20];
			BE = new double[vocSize][20];
			
			for (int k = 0; k < vocSize; k++) {
				for (int j = 0; j < 20; j++) {
					MLE[k][j] = (double) groups[j].wordCounts[k] / groups[j].totalNumOfWords;
					BE[k][j] = ((double) groups[j].wordCounts[k] + 1) / (groups[j].totalNumOfWords + vocSize);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	public void loadTestingData(String labelFilePath, String dataFilePath) throws IOException {

		try {
			
			loadLableFile(labelFilePath);
			loadDataFile(dataFilePath);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	// read label file
	private void loadLableFile(String labelFilePath) throws IOException {
		br = new BufferedReader(new FileReader(labelFilePath));
		groups = new NewsGroup[20];
		int[] groupDocs = new int[20];

		while ((line = br.readLine()) != null)
			groupDocs[Integer.parseInt(line) - 1]++;

		totalNumOfDocs = IntStream.of(groupDocs).sum();

		for (int i = 0; i < 20; i++) {
			groups[i] = new NewsGroup();
			groups[i].prior = (double) groupDocs[i] / totalNumOfDocs;
			groups[i].startDocIdx = i == 0 ? 1 : groups[i - 1].endDocIdx + 1;
			groups[i].endDocIdx = groups[i].startDocIdx + groupDocs[i] - 1;
		}
	}

	// read vocabulary file
	private void loadVocFile(String vocFilePath) throws IOException {
		br = new BufferedReader(new FileReader(vocFilePath));
		while ((line = br.readLine()) != null)
			vocSize++;
	}

	// read data file
	private void loadDataFile(String dataFilePath) throws IOException {
		br = new BufferedReader(new FileReader(dataFilePath));
		docs = new Document[totalNumOfDocs];
		int groupIdx = 0;

		while ((line = br.readLine()) != null) {
			int[] temp = Arrays.stream(line.split(",")).mapToInt(Integer::parseInt).toArray();

			if (temp[0] > groups[groupIdx].endDocIdx)
				groupIdx++;
			// store docs
			if (docs[temp[0] - 1] == null)
				docs[temp[0] - 1] = new Document();
			docs[temp[0] - 1].words.put(temp[1] - 1, temp[2]);
			// store nk
			if (groups[groupIdx].wordCounts == null)
				groups[groupIdx].wordCounts = new int[vocSize];
			groups[groupIdx].wordCounts[temp[1] - 1] += temp[2];
			// store n
			groups[groupIdx].totalNumOfWords += temp[2];
		}
	}

	// predict all docs and return overall accuracy
	public double predictDocs(Document[] docs, double[][] estimator) {
		CM = new int[20][20];
		int correct = 0;
		int totalCorrect = 0;
		int nb = 0;
		// each group
		for (int j = 0; j < 20; j++) {
			correct = 0;
			// each doc
			for (int i = groups[j].startDocIdx - 1; i < groups[j].endDocIdx; i++) {
				nb = predictDoc(docs[i].words, estimator);
				if (nb == j)
					correct++;
				CM[j][nb]++;
			}
			groups[j].groupAccuracy = (double) correct / (groups[j].endDocIdx - groups[j].startDocIdx + 1);
			totalCorrect += correct;
		}
		return (double) totalCorrect / totalNumOfDocs;
	}

	// predict each doc and return true if prediction matches reality
	private int predictDoc(HashMap<Integer, Integer> word, double[][] estimator) {
		double max = Double.NEGATIVE_INFINITY;
		int nb = 0;
		double sum = 0;
		for (int j = 0; j < 20; j++) {
			sum = 0;
			for (Integer i : word.keySet())
				sum += Math.log(estimator[i][j]) * word.get(i);
			double temp = (sum + Math.log(groups[j].prior));
			if (temp > max) {
				nb = j;
				max = temp;
			}
		}
		return nb;
	}
}
