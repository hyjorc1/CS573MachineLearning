import java.io.IOException;

public class test {

	public static void main(String[] args) throws IOException {

		String trainLabel = "U:/git/CS573MachineLearning/NaiveBayesAlgorithm/src/train_label.csv";
		String trainData = "U:/git/CS573MachineLearning/NaiveBayesAlgorithm/src/train_data.csv";
		String voc = "U:/git/CS573MachineLearning/NaiveBayesAlgorithm/src/vocabulary.txt";
		String testLabel = "U:/git/CS573MachineLearning/NaiveBayesAlgorithm/src/test_label.csv";
		String testData = "U:/git/CS573MachineLearning/NaiveBayesAlgorithm/src/test_data.csv";

		if (args.length == 5) {
			trainLabel = args[0];
			trainData = args[1];
			voc = args[2];
			testLabel = args[3];
			testData = args[4];
		}

		NaiveBayes n = new NaiveBayes();
		n.learnTrainingData(trainLabel, trainData, voc);

		// groups
		for (int i = 0; i < 20; i++) {
			System.out.println(
					"group " + (i + 1) + ": prior = " + n.groups[i].prior + " range: (" + n.groups[i].startDocIdx + ", "
							+ n.groups[i].endDocIdx + ")" + " NumOfWords: " + n.groups[i].totalNumOfWords);
		}

		System.out.println("\n" + "group 1 first 20 words count: ");
		printCountArr(n.groups[0].wordCounts);

		System.out.println("\n" + "group 20 first 20 words count: ");
		printCountArr(n.groups[19].wordCounts);

		System.out.println("\n" + "\n" + "MLE first 5 words:");
		print2DDouble(n.MLE);
		System.out.println("\n" + "BE first 5 words:");
		print2DDouble(n.BE);

		// training data performance
		performance(n, n.BE, "training data");
		performance(n, n.MLE, "training data");

		// testing data performance
		n.loadTestingData(testLabel, testData);
		performance(n, n.BE, "testing data");
		performance(n, n.MLE, "testing data");
	}

	static void performance(NaiveBayes n, double[][] estimator, String docs) {

		String est = estimator.equals(n.MLE) ? "MLE" : "BE";

		System.out.println("\n");
		System.out.println("Use " + est + " on " + docs);
		System.out.println("OverAll Accuracy: " + n.predictDocs(n.docs, estimator));

		System.out.println("Class Accuracy");
		for (int i = 0; i < 20; i++) {
			System.out.println("group " + (i + 1) + ": " + n.groups[i].groupAccuracy);
		}

		System.out.println("\n" + "Confusion Matrix:");
		print2DInt(n.CM);
	}

	static void print2DDouble(double[][] mat) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < mat[i].length; j++) {
				System.out.print(mat[i][j] + " ");
			}
			System.out.println();
		}
	}

	static void print2DInt(int[][] cM) {
		for (int i = 0; i < cM.length; i++) {
			for (int j = 0; j < cM[i].length; j++) {
				System.out.print(cM[i][j] + " ");
			}
			System.out.println();
		}
	}

	static void printCountArr(int[] arr) {
		for (int i = 0; i < 20; i++)
			System.out.print(arr[i] + " ");
	}
}
