/**
 * 
 * @author Yijia Huang
 *
 */
public class NewsGroup {
	int startDocIdx;
	int endDocIdx;
	int totalNumOfWords;
	int[] wordCounts;
	double prior;
	double groupAccuracy;
	public NewsGroup() {
		startDocIdx = 0;
		endDocIdx = 0;
		prior = 0.0;
		totalNumOfWords =0;
		groupAccuracy = 0;
	}
}
